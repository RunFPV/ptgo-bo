'use strict';

const fs = require('fs');
const OAuth = require('./src/oauth');
var cors = require('cors');

if (module === require.main) {
	require('dotenv').config();

	const express = require('express');
	const crypto = require('crypto');
	const qs = require('querystring');

	const app = express();

	// CORS
	var cors = function (req, res, next) {
		var whitelist = [
			'http://192.168.1.30',
			'http://ptgo.ddns.net',
			'https://192.168.1.30',
			'https://ptgo.ddns.net'
		];
		var origin = req.headers.origin;
		if (whitelist.indexOf(origin) > -1) {
			res.setHeader('Access-Control-Allow-Origin', origin);
		}
		res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
		res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
		next();
	}
	app.use(cors);

	const buffer = crypto.randomBytes(16);
	const state = buffer.toString('hex');

	const twitchOAuth = new OAuth({
		oauth_name: 'twitch',
		client_id: process.env.CLIENT_ID,
		client_secret: process.env.CLIENT_SECRET,
		redirect_uri: process.env.REDIRECT_URI,
		authorize_url: 'https://id.twitch.tv/oauth2/authorize',
		token_url: 'https://id.twitch.tv/oauth2/token',
		scopes: ['viewing_activity_read', 'user_read']
	}, state);

	const streamlabsOAuth = new OAuth({
		oauth_name: 'streamlabs',
		client_id: process.env.SL_CLIENT_ID,
		client_secret: process.env.SL_CLIENT_SECRET,
		redirect_uri: process.env.SL_REDIRECT_URI,
		authorize_url: 'https://streamlabs.com/api/v1.0/authorize',
		token_url: 'https://streamlabs.com/api/v1.0/token',
		scopes: ['alerts.create']
	}, state)

	app.get('/', (req, res) => {
		res.redirect(twitchOAuth.authorizeUrl);
	});

	app.get('/home', (req, res) => {
		res.status(200).send(`<a href="/viewer-count">ViewerCount</a></br><a href="/test-alert">TestAlert</a>`);
	});

	app.get('/test-alert', async (req, res) => {
		let url = `https://api.twitch.tv/helix/users`;

		try {
			let json = await twitchOAuth.getEndpoint(url);
			let response = {};
			const userId = json.data[0].id;

			url = `https://api.twitch.tv/helix/streams?user_id=` + userId;
			json = await twitchOAuth.getEndpoint(url);

			if (json.data.length < 1) {
				response.info = "No stream found for the user : " + userId;
				res.status(200).json({ response });
			}
			else {
				const viewerCount = json.data[0].viewer_count;

				// SENDING TEST ALERT
				const urlParams = [
					`type=donation`,
					`message=${encodeURIComponent("Test titre")}`,
					`user_message=${encodeURIComponent("Test message")}`
				];

				const urlQuery = urlParams.join('&');
				url = `https://streamlabs.com/api/v1.0/alerts?${urlQuery}`;

				response.viewers = viewerCount;

				res.status(200).json({ response });
			}

		} catch (err) {
			console.error(err);
			res.redirect('/failed');
		}
	});

	app.get('/viewer-count', async (req, res) => {
		let url = `https://api.twitch.tv/helix/users`;
		//let url = `https://api.twitch.tv/helix/users?login=soprasteria`;

		try {
			let json = await twitchOAuth.getEndpoint(url);
			let response = {};
			const userId = json.data[0].id;

			url = `https://api.twitch.tv/helix/streams?user_id=` + userId;
			json = await twitchOAuth.getEndpoint(url);

			if (json.data.length < 1) {
				response.info = "No stream found for the user : " + userId;
				res.status(200).json({ response });
			}
			else {
				const viewerCount = json.data[0].viewer_count;

				// VIEWER GOALS
				var giveawayNbre = -1;
				if (fs.existsSync('giveaway.txt')) {
					giveawayNbre = fs.readFileSync('giveaway.txt').length;
				}

				var isNewGoal = false;

				const urlParams = [
					`type=donation`,
				];

				// test

				//if (giveawayNbre === 0 && viewerCount > -1) {
				//	fs.appendFileSync('giveaway.txt', '+');
				//	urlParams.push(`message=${encodeURIComponent("Giveaway 50 €")}`);
				//	urlParams.push(`user_message=${encodeURIComponent("Prochain objectif, 50 viewers")}`);
				//	isNewGoal = true;
				//}

				// fintest

				if (giveawayNbre === 0 && viewerCount > 24) {
					fs.appendFileSync('giveaway.txt', '+');
					urlParams.push(`message=${encodeURIComponent("Giveaway 50 €")}`);
					urlParams.push(`user_message=${encodeURIComponent("Prochain objectif, 50 viewers")}`);
					isNewGoal = true;
				}

				if (giveawayNbre === 1 && viewerCount > 49) {
					fs.appendFileSync('giveaway.txt', '+');
					urlParams.push(`message=${encodeURIComponent("Giveaway 100 €")}`);
					urlParams.push(`user_message=${encodeURIComponent("Prochain objectif, 75 viewers")}`);
					isNewGoal = true;
				}

				if (giveawayNbre === 2 && viewerCount > 74) {
					fs.appendFileSync('giveaway.txt', '+');
					urlParams.push(`message=${encodeURIComponent("Giveaway 150 €")}`);
					urlParams.push(`user_message=${encodeURIComponent("Prochain objectif, 100 viewers")}`);
					isNewGoal = true;
				}

				if (giveawayNbre === 3 && viewerCount > 99) {
					fs.appendFileSync('giveaway.txt', '+');
					urlParams.push(`message=${encodeURIComponent("Giveaway 200 €")}`);
					urlParams.push(`user_message=${encodeURIComponent("Prochain objectif, 150 viewers")}`);
					isNewGoal = true;
				}

				if (giveawayNbre === 4 && viewerCount > 149) {
					fs.appendFileSync('giveaway.txt', '+');
					urlParams.push(`message=${encodeURIComponent("Giveaway 250 €")}`);
					urlParams.push(`user_message=${encodeURIComponent("Tous les objectifs sont atteints, BRAVO à VOUS !")}`);
					isNewGoal = true;
				}

				const urlQuery = urlParams.join('&');
				url = `https://streamlabs.com/api/v1.0/alerts?${urlQuery}`;

				response.viewers = viewerCount;

				// New goal unlocked
				if (isNewGoal) {
					const resp = await streamlabsOAuth.postEndpoint(url);

					if (resp.success) {
						response.info = "GOAL UNLOCKED !!!"
					} else {
						response.info = "Error : " + resp.toString();
					}
				} else {
					response.info = "No goal unlocked";
				}
				res.status(200).json({ response });
			}

		} catch (err) {
			console.error(err);
			res.redirect('/failed');
		}
	});

	app.get('/auth-callback', async (req, res) => {
		const req_data = qs.parse(req.url.split('?')[1]);
		const code = req_data['code'];
		const state = req_data['state'];

		try {
			twitchOAuth.confirmState(state);
			await twitchOAuth.fetchToken(code);
			console.log('Twitch authenticated');
			res.redirect(streamlabsOAuth.authorizeUrl);
		} catch (err) {
			console.error(err);
			res.redirect('/failed');
		}
	});

	app.get('/sl-auth-callback', async (req, res) => {
		const req_data = qs.parse(req.url.split('?')[1]);
		const code = req_data['code'];
		const state = req_data['state'];

		try {
			streamlabsOAuth.confirmState(state);
			await streamlabsOAuth.fetchToken(code);
			console.log('Streamlabs authenticated');
			res.redirect('/home');
		} catch (err) {
			console.error(err);
			res.redirect('/failed');
		}
	});

	const server = app.listen(process.env.PORT || 4000, () => {
		const port = server.address().port;
		console.log(`Server listening on port ${port}`);

		const url = twitchOAuth.authorizeUrl;
		const open = require('open');
		open(url);
	});
}

module.exports = OAuth;
